package main

import (
	"context"
	"flag"
	"fmt"
	"log"

	"git.ozon.dev/mfatkhutdinova/3.10-observability/pkg/fibonacci-service"

	"google.golang.org/grpc"
	"google.golang.org/grpc/grpclog"
)

func main() {
	port := flag.Int("port", 8080, "grpc server port")

	opts := []grpc.DialOption{
		grpc.WithInsecure(),
	}
	//args := os.Args
	conn, err := grpc.Dial(fmt.Sprintf(":%d", *port), opts...)
	if err != nil {
		grpclog.Fatalf("fail to dial: %v", err)
	}

	defer conn.Close()

	client := fibonacci_service.NewFibonacciClient(conn)
	request := &fibonacci_service.Request{
		Number: 100, //args[1],
	}

	response, err := client.Get(context.Background(), request)

	if err != nil {
		log.Println("Error from grpc server: ", err)
	}

	log.Println("Response: ", response.FibonacciNumber)
}
