package main


import (
	"flag"
	"fmt"
	"git.ozon.dev/mfatkhutdinova/3.10-observability/internal/fibonacci"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"log"
	"net"
	"net/http"

	fb "git.ozon.dev/mfatkhutdinova/3.10-observability/pkg/fibonacci-service"
	"github.com/uber/jaeger-client-go/config"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"google.golang.org/grpc"
)

var (
	port     = flag.Int("port", 8080, "port to listen on")
	logLevel = zap.LevelFlag("log.level", zapcore.DebugLevel, "log level")
)

func main() {
	flag.Parse()

	tcfg := config.Configuration{
		Sampler: &config.SamplerConfig{
			Type:  "const",
			Param: 1,
		},
	}

	closer, err := tcfg.InitGlobalTracer("fibonacci")
	if err != nil {
		log.Fatal(err)
	}
	defer closer.Close()

	cfg := zap.NewProductionConfig()
	cfg.DisableCaller = true
	cfg.DisableStacktrace = true
	cfg.Level = zap.NewAtomicLevelAt(*logLevel)
	logger, err := cfg.Build()
	if err != nil {
		log.Fatal(err)
	}

	logger.Debug("starting server...", zap.Int("port", *port))
	listener, err := net.Listen("tcp", fmt.Sprintf(":%d", *port))
	if err != nil {
		logger.Fatal("failed to listen: %v", zap.Error(err))
	}

	s := fibonacci.NewFibonacciService(logger)
	grpcServer := grpc.NewServer()

	fb.RegisterFibonacciServer(grpcServer, s)

	if err := grpcServer.Serve(listener); err != nil {
		log.Fatalf("failed to serve: %s", err)
	}

	http.Handle("/metrics", promhttp.Handler())

	if err := http.ListenAndServe(fmt.Sprintf(":%v", *port), nil); err != nil {
		logger.Error("failed to start server", zap.Error(err))
	}
}
