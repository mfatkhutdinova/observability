package fibonacci

import (
	"context"

	"github.com/opentracing/opentracing-go"
)

func Get(ctx context.Context, n int) int {
	span, ctx := opentracing.StartSpanFromContext(ctx, "getNumber")
	span.SetTag("n", n)
	defer span.Finish()
	switch {
	case n == 0:
		return 0
	case n == 1:
		return 1
	default:
		return Get(ctx, n-1) + Get(ctx, n-2)
	}
}
